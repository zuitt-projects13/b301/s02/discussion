package com.zuitt.example;
// import java.util.*; mas mabagal to kasi marami sya need iload
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Collections {
    public static void main(String[] args) {
        // ! Arrays are fixed-size data structure that store a collection of elements of the same type

        // 5 - size
        int[] intArray = new int[5];
        System.out.println("Initial state of intArray: ");
        System.out.println(Arrays.toString(intArray));

        intArray[0] =1;
        intArray[intArray.length-1] = 5;
        System.out.println("Update Array: ");
        System.out.println(Arrays.toString(intArray));

        // could not add elements because the length is already fixed
//        intArray[intArray.length] =6;

        System.out.println("-------------------------------");
        // ! Multidimensional arrays
        // 3 rows 3 columns
        String[][]  classroom = new String[3][3];

        // first row
        classroom[0][0] = "Arthos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        // second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";

        // third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofey";

        System.out.println(Arrays.deepToString(classroom));



        System.out.println("-------------------------------");
        // ! ArrayList

        ArrayList<String> students = new ArrayList<>(); // diamond function
        System.out.println(students);
        System.out.print(students); // this will display the next line next to the result

        students.add("Jonh");
        students.add("Paul");
        System.out.println(students.size()); // 2
        System.out.println(students.get(0)); // John
        System.out.println(students);

        students.set(1,"George");
        System.out.println(students);

        students.remove(1);
        System.out.println(students);

        // reset array
        students.clear();
        System.out.println(students.size());
        System.out.println(students);


        System.out.println("-------------------------------");
        // ! HashMaps
        // key value pair; unique keys

        HashMap<String, String> job_position = new HashMap<>();
        job_position.put("Brandon", "Student");
        job_position.put("Alice", "Dreamer");

        // use replace for updating
        job_position.replace("Brandon", "Test");
        System.out.println(job_position);
        System.out.println(job_position.get("Alice"));

        //display all keys
        System.out.println(job_position.keySet());

        //display all values
        System.out.println(job_position.values());

        job_position.remove("Brandon"); // removes the key value pair
        System.out.println(job_position);

//        job_position.entrySet()

    }
}
